@extends('layouts.master')

@section('content')
<div class="container d-flex align-items-center flex-column">
    <br>
    <br>
    <br>
    <br>
    <h2>Ini pengumuman</h2>
    <p>
        Untuk menjadi seorang blogger yang baik, 
        kemampuan blogging saja tidak cukup. Anda
         perlu mengoptimasi SEO supaya performa blog 
         lebih maksimal. Salah satu caranya yaitu dengan memahami coding HTML dasar untuk blogger.
    </p>
    <p>Dengan mengetahui tag HTML untuk blog, Anda bisa dengan bebas menyusun informasi di dalam konten. Hal ini membuat pengunjung dan bot lebih mudah menemukan informasi di konten Anda. 

        Hasilnya, konten blog Anda akan punya kemungkinan besar masuk halaman #1 Google. Bahkan, bisa jadi cuplikan konten Anda muncul dengan tampilan yang lebih menarik. Hal ini tentunya bisa mengundang lebih banyak klik ke blog.
        
        Tapi jangan cemas dulu karena menganggap coding itu sulit. Di sini, Anda akan belajar coding HTML dasar yang sederhana dan mudah untuk digunakan. Apa saja kah itu? Yuk simak penjelasannya!</p>
    <h1></h1>
</div
@endsection