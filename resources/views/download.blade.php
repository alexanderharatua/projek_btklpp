@extends('layouts.master')

@section('content')
<section class="page-section" style="padding-bottom: 90px">
    <h4 class="text-center text-uppercase text-secondary mb-0">Download File</h4>
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Dokumen</th>
                    <th scope="col">Dokumen</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>
   
</section>
   

@endsection